/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.fire.ingestion.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ebi.ega.fire.exceptions.ClientProtocolException;
import uk.ac.ebi.ega.fire.exceptions.FireServiceException;
import uk.ac.ebi.ega.fire.listener.ProgressListener;
import uk.ac.ebi.ega.fire.models.ErrorResponse;
import uk.ac.ebi.ega.fire.models.FileBodyInterceptor;
import uk.ac.ebi.ega.fire.models.FireObjectRequest;
import uk.ac.ebi.ega.fire.models.FireObjectResponse;
import uk.ac.ebi.ega.fire.models.FireResponse;
import uk.ac.ebi.ega.fire.models.IFireResponse;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.time.LocalDateTime;
import java.util.Optional;

import static uk.ac.ebi.ega.fire.utils.FireUtils.getObjectMapper;

public class FireService implements IFireServiceNew {

    private static final Logger LOGGER = LoggerFactory.getLogger(FireService.class);

    private static final String PATH_OBJECTS = "objects";
    private static final String PATH_OBJECTS_QUERY_BY_PATH = "objects/path/";

    private final String fireURL;
    private final CloseableHttpClient httpClient;

    public FireService(final CloseableHttpClient httpClient, final String fireURL) {
        this.httpClient = httpClient;
        if (fireURL.endsWith("/")) {
            this.fireURL = fireURL;
        } else {
            this.fireURL = fireURL + "/";
        }
    }

    @Override
    public boolean deleteByPath(String firePath) throws FireServiceException, ClientProtocolException {
        final Optional<FireResponse> file = findFile(firePath);
        return file.isPresent() && deleteById(file.get().getFireOid());
    }

    @Override
    public boolean deleteById(String fireOid) throws ClientProtocolException, FireServiceException {
        // build http request and assign multipart upload data
        final HttpUriRequest request = RequestBuilder
                .delete(fireURL + PATH_OBJECTS + "/" + fireOid)
                .build();

        try (final CloseableHttpResponse httpResponse = httpClient.execute(request)) {
            final int status = httpResponse.getStatusLine().getStatusCode();
            switch (status) {
                case 200:
                case 204:
                    return true;
                case 401:
                case 403:
                    throw new ClientProtocolException(getObjectMapper().readValue(
                            EntityUtils.toString(httpResponse.getEntity()), ErrorResponse.class));
                case 404:
                    return false;
                default:
                    LOGGER.error("status: {}", status);
                    throw new ClientProtocolException(getObjectMapper().readValue(
                            EntityUtils.toString(httpResponse.getEntity()), ErrorResponse.class));
            }
        } catch (IOException e) {
            throw new FireServiceException("Unable to execute request. Can be retried.", e);
        }
    }

    @Override
    public Optional<FireResponse> findFile(String firePath) throws FireServiceException, ClientProtocolException{
        while (firePath.startsWith("/")) {
            firePath = firePath.substring(1);
        }
        // build http request and assign multipart upload data
        final HttpUriRequest request = RequestBuilder
                .get(fireURL + PATH_OBJECTS_QUERY_BY_PATH + firePath)
                .build();

        try (final CloseableHttpResponse httpResponse = httpClient.execute(request)) {
            return buildResponseFindFile(httpResponse);
        } catch (IOException e) {
            throw new FireServiceException("Unable to execute request. Can be retried.", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IFireResponse upload(final FireObjectRequest fireObjectRequest,
                                final ProgressListener progressListener) throws FireServiceException,
            ClientProtocolException, FileAlreadyExistsException {
        LOGGER.info("Data received to upload file = {}", fireObjectRequest);
        try {
            return doUpload(fireObjectRequest, progressListener);
        } catch (ClientProtocolException ex) {
            //all errors related to invalid request
            throw ex;
        } catch (FileAlreadyExistsException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error("Error while uploading file", e);
            //all errors/exceptions apart from invalid request E.g. network issues etc.
            throw new FireServiceException("Unable to upload file; Network issue will be the likely cause. Can be " +
                    "retried in some time.", e);
        }
    }

    private IFireResponse doUpload(final FireObjectRequest fireObjectRequest,
                                   final ProgressListener progressListener) throws ClientProtocolException, IOException {
        final FileBodyInterceptor fileBodyInterceptor = new FileBodyInterceptor(fireObjectRequest.getFileToUpload(), progressListener);

        // build multipart upload request
        final HttpEntity httpEntity = MultipartEntityBuilder.create()
                .addPart("file", fileBodyInterceptor)
                .build();

        // build http request and assign multipart upload data
        final HttpUriRequest request = RequestBuilder
                .post(fireURL + PATH_OBJECTS)
                .addHeader("x-fire-size", String.valueOf(fireObjectRequest.getFileToUpload().length()))
                .addHeader("x-fire-md5", fireObjectRequest.getMd5())
                .addHeader("x-fire-path", fireObjectRequest.getFirePath())
                .setEntity(httpEntity)
                .build();

        LOGGER.info("Process for file {} started at {}. File length is {}", fireObjectRequest.getFileToUpload().getAbsolutePath(),
                LocalDateTime.now(), fireObjectRequest.getFileToUpload().length());

        try (final CloseableHttpResponse httpResponse = httpClient.execute(request)) {
            final IFireResponse fireResponse = buildResponsePostFile(httpResponse);
            LOGGER.info("Process for file {} ended at {}. Total {} bytes transferred", fireObjectRequest.getFileToUpload().getAbsolutePath(),
                    LocalDateTime.now(), fileBodyInterceptor.getBytesWritten());
            return fireResponse;
        }
    }

    /**
     * Maps Server response to object specified. It is expected to have an Object structure same as
     * response returned from the server. Mapping happens in both success & failure.
     * <p>
     * Logic can be moved to another class if needed; for the time being it is integral part of this service.
     *
     * @param httpResponse HttpResponse received from server.
     * @return Object of type IFireResponse.
     * @throws IOException             throws IOException in case of error while processing JSON.
     * @throws ClientProtocolException throws when bad request has been sent.
     */
    private IFireResponse buildResponsePostFile(final HttpResponse httpResponse) throws ClientProtocolException, IOException {
        final int status = httpResponse.getStatusLine().getStatusCode();
        switch (status) {
            case 200:
            case 201:
                //Condition can be status >= 200 && status < 300. Expected response is 200.
                return new FireResponse(getObjectMapper()
                        .readValue(EntityUtils.toString(httpResponse.getEntity()), FireObjectResponse.class));
            case 409:
                throw new FileAlreadyExistsException("File already exists on the given fire path");
            case 401:
            case 403:
                throw new ClientProtocolException(getObjectMapper().readValue(
                        EntityUtils.toString(httpResponse.getEntity()), ErrorResponse.class));
            case 503:
                throw new ClientProtocolException(ErrorResponse.error503("POST"));
            default:
                LOGGER.error("status: {}", status);
                throw new ClientProtocolException(getObjectMapper().readValue(
                        EntityUtils.toString(httpResponse.getEntity()), ErrorResponse.class));
        }
    }

    private Optional<FireResponse> buildResponseFindFile(final HttpResponse httpResponse) throws ClientProtocolException,
            IOException {
        final int status = httpResponse.getStatusLine().getStatusCode();
        switch (status) {
            case 200:
                //Condition can be status >= 200 && status < 300. Expected response is 200.
                return Optional.of(new FireResponse(getObjectMapper()
                        .readValue(EntityUtils.toString(httpResponse.getEntity()), FireObjectResponse.class)));
            case 401:
            case 403:
                //TODO unauthorized?
                throw new ClientProtocolException(getObjectMapper().readValue(
                        EntityUtils.toString(httpResponse.getEntity()), ErrorResponse.class));
            case 404:
                return Optional.empty();
            case 503:
                throw new ClientProtocolException(ErrorResponse.error503("GET"));
            default:
                LOGGER.error("status: {}", status);
                throw new ClientProtocolException(getObjectMapper().readValue(
                        EntityUtils.toString(httpResponse.getEntity()), ErrorResponse.class));
        }
    }

}