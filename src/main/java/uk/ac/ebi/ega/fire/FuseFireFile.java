/*
 *
 * Copyright 2018 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.fire;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class FuseFireFile implements IFireFile {

    private final File file;

    public FuseFireFile(String firePath, String submittedFileName) throws FileNotFoundException {
        this.file = new File(firePath + submittedFileName);
        if (!file.exists()) {
            throw new FileNotFoundException("File " + file.getAbsolutePath() + " could not be found");
        }
    }

    @Override
    public long getSize() {
        return file.length();
    }

    @Override
    public String getMd5() {
        return null;
    }

    @Override
    public InputStream getStream() throws FileNotFoundException {
        return new FileInputStream(file);
    }

}
