package uk.ac.ebi.ega.fire.models;

public class OldFireFile {

    private final long fireOid;

    private final long size;

    private final String path;

    private final Integer exitCode;

    private final String exitReason;

    public OldFireFile(final long fireOid,
                       final long size,
                       final String path,
                       final Integer exitCode,
                       final String exitReason) {
        this.fireOid = fireOid;
        this.size = size;
        this.path = path;
        this.exitCode = exitCode;
        this.exitReason = exitReason;
    }

    /**
     * @return fireId the value of the "ega-pro-filer.ega_ARCHIVE.archive.archive_id" column.
     */
    public Long getFireOid() {
        return fireOid;
    }

    public long getSize() {
        return size;
    }

    public String getPath() {
        return path;
    }

    public Integer getExitCode() {
        return exitCode;
    }

    public String getExitReason() {
        return exitReason;
    }

    @Override
    public String toString() {
        return "OldFireFile{" +
                "fireOid=" + fireOid +
                ", size=" + size +
                ", path='" + path + '\'' +
                ", exitCode=" + exitCode +
                ", exitReason='" + exitReason + '\'' +
                '}';
    }

}
